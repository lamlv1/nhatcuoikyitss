import './App.css';
import useStorage from './hooks/storage';
import { useState } from 'react';

function App() {
  const { data, arr, deleteName } = useStorage({
    initData: ["Huyen", "Hoa", "Hung", "Long"]
  })
  const [name, setName] = useState("");
  return (
    <div className="App">
      <h3>学生一覧: [{data.toString()}]</h3>
      <h3>削除を入力してください。</h3>
      <div><span style={{ "font-weight": "700" }}>検索名前:</span>
        <input style={{ "margin-left": "10px" }} type="text" value={name} onChange={(e) => setName(e.target.value)}></input>
        <button style={{ "margin-left": "10px" }} type="button" onClick={() => deleteName(name)}>確定</button>
      </div>
      <div style={{ "padding-top": "10px" }}>
        <span style={{ "font-weight": "700" }}>削除する名前:</span>{name}
      </div>
      <div style={{ "padding-top": "10px" }}>
        <span style={{ "font-weight": "700", "margin-right": "10px" }}>新しい一覧:</span>{`[${arr.toString()}]`}
      </div>
    </div>
  );
}

export default App;